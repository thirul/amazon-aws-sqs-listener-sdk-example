package com.boomi.connector.awssqslistener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.util.BaseConnection;

public class AWSSQSListenerConnection extends BaseConnection {

	public AWSSQSListenerConnection(BrowseContext context) {
		super(context);
	}
	
}