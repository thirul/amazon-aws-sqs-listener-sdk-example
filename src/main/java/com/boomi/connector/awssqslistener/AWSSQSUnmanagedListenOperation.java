package com.boomi.connector.awssqslistener;

import java.beans.ExceptionListener;
import java.io.ByteArrayInputStream;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.util.StringUtils;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.util.listen.UnmanagedListenOperation;
import com.boomi.util.LogUtil;

public class AWSSQSUnmanagedListenOperation extends UnmanagedListenOperation implements MessageListener , ExceptionListener{

    private static final Logger logger = LogUtil.getLogger(AWSSQSUnmanagedListenOperation.class);
    Listener _listener;
    PropertyMap connProps;
    PropertyMap opProps;
    SQSConnection _connection;
//    private String _topic;
//    private Workload _workload;

    /**
     * Creates a new instance using the provided operation context
     */
    protected AWSSQSUnmanagedListenOperation(OperationContext context) {
        super(context);
        logger.info("Constructing AWSSQSUnmanagedListenOperation");
    }

    @Override
    protected void start(Listener listener) {
        logger.info("AWSSQSUnmanagedListenOperation.start");
        _listener = listener;
        connProps = getContext().getConnectionProperties();
        opProps = getContext().getOperationProperties();
     // Create a new connection factory with all defaults (credentials and region) set automatically
         
        // Create the connection.
		try {
	        ProviderConfiguration providerConfiguration = new ProviderConfiguration();
			//        providerConfiguration.setNumberOfMessagesToPrefetch(numberOfMessagesToPrefetch);
	        AmazonSQSClientBuilder sqs = AmazonSQSClientBuilder.standard();
	        String region = connProps.getProperty("AWSREGION");
	        sqs.setRegion(region);
	        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
	        		providerConfiguration,
	        		sqs
	                );
			String awsAccessKeyId = connProps.getProperty("AWSACCESSKEYID");
	        if (StringUtils.isNullOrEmpty(awsAccessKeyId))
	        	throw new ConnectorException("AWSACCESSKEYID connection parameter must be specified");
			String awsSecretKey = connProps.getProperty("AWSSECRETKEY");
	        if (StringUtils.isNullOrEmpty(awsSecretKey))
	        	throw new ConnectorException("AWSSECRETKEY connection parameter must be specified");
			_connection = connectionFactory.createConnection(awsAccessKeyId, awsSecretKey);
		     // Create the nontransacted session with AUTO_ACKNOWLEDGE mode
			int acknowledgeMode = opProps.getLongProperty("ACKNOWLEDGE_MODE", 1L).intValue();
	        Session session = _connection.createSession(false, acknowledgeMode);
	        String queueName = opProps.getProperty("QUEUENAME");
	        if (StringUtils.isNullOrEmpty(queueName))
	        	throw new ConnectorException("QUEUE Name operation parameter must be specified");
	        Queue queue = session.createQueue(queueName);
	        // Create a consumer for the 'MyQueue'
	         MessageConsumer consumer = session.createConsumer(queue);
	      // Instantiate and set the message listener for the consumer.
	         consumer.setMessageListener(this);
	         // Start receiving incoming messages
	         _connection.start();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConnectorException(e);
		}    
        
         logger.info("AWSSQSUnmanagedListenOperation.start complete");
    }

    /**
     * Stops the listen operation by deregistering with the manager
     */
    @Override
    public void stop() {
        logger.info("AWSSQSUnmanagedListenOperation.stop");
        close();
    }     

	@Override
	public void exceptionThrown(Exception e) {
        logger.info("AWSSQSUnmanagedListenOperation.exceptionThrown: " + e.getMessage());
		_listener.submit(e);
		this.stop();
	}

	@Override
	public void onMessage(Message message) {
    	PayloadMetadata payloadMetadata = this.getContext().createMetadata();
//    	payloadMetadata.setTrackedProperty("mqttTopicIn", topic);
    	try {
    		String messageId = message.getJMSMessageID();
    		payloadMetadata.setTrackedProperty("MESSAGE_ID", messageId);
    		Enumeration propertyNames = message.getPropertyNames();
    		while (propertyNames.hasMoreElements())
    		{
    			//TODO only string properties supported...can we cast all other types to string? How do we know which to get?
    			String propName = (String) propertyNames.nextElement();
    			String value = message.getStringProperty(propName);
    			payloadMetadata.setUserDefinedProperty(propName, value);
    		}
//    				message.getPropertyNames();
//    				message.acknowledge(); //TODO do we wait for Future to ack if process OK?
//TODO need a Properties DP that includes name/value pairs....delimited by???? 
//    		Queue Name
//    		Message Id
//    		Receipt Handle
//    		Sequence Number
//    		Message Group Id
//    		Message Deduplication Id
			Payload payload = PayloadUtil.toPayload(((TextMessage)message).getText(), payloadMetadata);
        	_listener.submit(payload);
		} catch (JMSException e) {
			e.printStackTrace();
        	_listener.submit(e);
		}	
	}
	
	private void close()
	{
        if (_connection!=null)
			try {
				_connection.close();
			} catch (JMSException e) {
				e.printStackTrace();
			}
	}
}