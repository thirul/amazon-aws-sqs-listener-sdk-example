# Amazon AWS SQS Listener

The AWS SQS Connector implements listening to an SQS FIFO Queue. It provides listen capabilities to augment the Receive/Send/Delete operations of the public Boomi SQS Connector described at:  https://help.boomi.com/bundle/connectors/page/r-atm-Amazon_SQS_connector.html

# Connection Tab

## Connection Fields


### AWS Region

The AWS Region for your SQS instance.

**Type** - string

**Default Value** - us-west-2




### AWS Access Key ID

Security credentials configured at IAM console: https://console.aws.amazon.com/iam/home#/users

**Type** - string




### AWS Secret Key

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - password


# Operation Tab



## Listen
### Operation Fields



#### Queue Name

Name of FIFO queue configured your SQS console. For us-east-2, that console is at: https://us-east-2.console.aws.amazon.com/sqs/v2/home?region=us-east-2#/queues

**Type** - string



# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.


# Outbound Document Properties

The connector does not support outbound document properties that can be read by a process after a connector shape.

